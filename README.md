# Docker Courses Pipeline
## Anforderungen
Im Root des Projektes muss eine `docker-compose.yml` Datei sein. Dabei sollte in der Compose ein `build` und ein `image` zu jedem Service vorhanden sein. Das `build` enthält dabei den Context und Path des jeweiligen Docker-Images. Das `image` sollte die Variable `$CI_REGISTRY_IMAGE` und `$VERSION` nutzen. Ein Beispiel einer Compose befindet sich in dem [Docker-Courses-Template](https://gitlab.com/hackademy-cloud/templates/docker-courses-template).

## Branches
- `master`: Der Master Branch erstellt eine neue Version und erstellt einmal ein latest Image und eins mit der Versionsnummer
- `testing`: Der Testing Branch erstellt das Testing Image

## Variablen
- `GITLAB_ACCESS_TOKEN`: Token mit Permission um ins Git Repo zu pushen. Wird genutzt für automatische Git-Tags.

## Aufbau
Die Pipeline hat 5 Stages.

### Commit auf `testing`
Ein Commit auf `testing` führt dazu, dass ein neues testing Image gebaut wird und in der Gitlab Container Registry gespeichert wird.

### Merge Request zu `master`
Wird ein Merge Request zu `master` gemacht, dann wird geprüft ob die Versionsnummer in der `version.txt` geändert wurde. Falls nicht, schlägt die Pipeline fehl und verhindert einen Merge in den master.

### Commit auf `master`
Bei einem neuen Commit auf `master` wird die master Pipeline gestartet. Diese beginnt damit, dass die Versionsnummer geprüft wurde wie im vorherigen Schritt "Merge Request zu master". Danach wird ein Git Tag hinzugefügt und zu Gitlab gepusht. Damit wollen wir bezwecken, dass die Git-Tags immer die gleichen Versionsnummern haben wie die Docker Tags. Nach dem Taggen und Pushen wird dann das Image gebaut und gepusht.
